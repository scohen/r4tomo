all: doc R/sysdata.rda

# build package documentation
doc:
	R -e 'devtools::document()'

# building the internal data for the package :
R/sysdata.rda: data-raw/DATASET.R data-raw/*.cl
	R -e 'source("data-raw/DATASET.R")'
