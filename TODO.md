* For function check.file : should add an dependencies argument. If the file exists but is older than the dependencies, it is still considered to be computed (do as if it does not exists).

* [DONE] Should provide defaults values for slots of classes (example in ufo.edge) : using initialize method
* [DONE] Should check validity of slot value using the setValidity functions

* [DONE] Creating generics addNode<-, addEdge<- and setImsize<-, then using the
* [DONE] setReplaceMethod call to edit the object /in place/

* [DONE] Transfert de la fonction de génération de cible (pour l'optimisation de la rotation, `create.rotation.target`) dans r4tomo (depuis wip_v11.org)

* Mise à jour de wip_v1.1.Rmd depuis wip_v1.1.org
* Transfert de la correction de rotation vers UFO
* Génération des sinogramme
* Reconstruction

* Pour le futur : utilisation d'autre techinique (SIFT ?) pour aligner deux images à 180°

 Lire : https://stackoverflow.com/questions/49132422/what-is-setreplacemethod-and-how-does-it-work

 Autres liens:
 * https://stackoverflow.com/questions/11563154/what-are-replacement-functions-in-r
 * http://adv-r.had.co.nz/S4.html

 # Autre :
 * [DONE] pour ufo.node : une fonction «copy changing name» (pour avec plusieurs /nodes/ identiques dans graph de traitement)
 * Utilisation de posix_spawn pour lancer ufo_runjson, et ainsi attendre de façon «semi-bloquante» la fin du traitement (éviter le manque de réactivité de l'interface pendant qu'UFO est en cours d'execution)
 * [DONE] Classe (S3) pour les matrices donnée en résultats de image.sequence, avec une fonction/méthode correspondante de =plot= qui fasse /tout comme il faut/ par défaut.
 * Converstion simple en r4tima et climage