#' @include generics.R
#' @include ufo.node.R
#' @include ufo.edge.R
#' @include raw.image.prop.R

#' A S4 class to represent an UFO processing job.
#'
#' @slot nodes The sets of nodes in the processing job
#' @slot edges The sets of edges (connecting nodes) in the processing job
#' @slot imsize The size of images processed by the job, mostly
#'              used as defaults when reading raw images.
#'
#' @export
setClass("ufo.job",
          slot=c(nodes="list",
                 edges="list",
                 imsize="raw.image.prop")
        )


#' The explicit constructor for ufo.job class
#'
#' @param nodes The sets of nodes in the processing job
#' @param edges The sets of edges (connecting nodes) in the processing job
#' @param imsize The size of images processed by the job, mostly
#'              used as defaults when reading raw images.
#'
#' @details Do nothing special, but creating a job
#'
#' @export
ufo.job <- function(nodes=list(), edges=list(), imsize) {
  if ( missing(imsize) ) {
    return(new("ufo.job", nodes=nodes, edges=edges))
  } else {
    return(new("ufo.job", nodes=nodes, edges=edges, imsize=imsize))
  }
}

### Also : getNode, replaceNode<-, setProp<- (by name)

#' A function to return all the names of the nodes of the job
#'
#' @param x The `ufo.job` from which to extract nodes' names
#'
#' @export
setMethod("names", signature(x="ufo.job"),
          function(x) {
              return(sapply(X=x@nodes, FUN=function(x) return(x@name)))
          })

#' A function to add a task to the process graph of an \code{ufo.job}
#'
#' @param job The job to which the node should be added
#' @param name If provided, the name of the node is changed upon addition
#'             to the job
#' @param value The node (of class \code{ufo.node}) to add to the job graph
#'
#' @details Later on, should check that the node being added does not have
#' an already used name
#'
#' @export
setMethod("addNode<-", signature(job="ufo.job", name="ANY", value="ufo.node"),
          function(job, name=NULL, value) {
            if ( ! is.null(name) ) { ## Changing node name just before addition
              value@name <- name
            }
            if (value@name %in% names(job)) {
              warning("The job already has a node names : ", value@name, ", we will not add this new node to the job")
            }
            else {
              job@nodes <- c(job@nodes, value)
            }
              return(job)
          })

#' A function to add an edge to the process graph of an \code{ufo.job}
#'
#' @param job The job to which the edge should be added
#' @param value The edge to add to the job graph
#'
#' @details maybe later should add a check that at least the node names
#' are present in the job, and a warning if this is not the case
#' (the model then would be to first add the nodes, then the edge).
#'
#' Maybe later it would be usefull to test that both ends of the added edge
#' correspond to nodes already present in the job.
#'
#' @export
setMethod("addEdge<-", signature(job="ufo.job", value="ufo.edge"),
          function(job, value) {
              job@edges <- c(job@edges, value)
              return(job)
          })

#' Setting the image size (raw) of an `ufo.job`
#'
#' @param job The job of which the imsize should be modified
#' @param value The new value for imsize
#'
#' @export
setMethod("setImsize<-", signature(x="ufo.job", value="raw.image.prop"),
          function(x, value) {
              x@imsize <- value
              return(x)
          })

#' A function to merge a job within a pre-existing job. This can be used
#' to include a composite task into a larger job
#'
#' @param job The job into which the composite task/sub-job should be imported
#' @param value The `ufo.job` (sub-job) that has to be included in the `job`
#'
#' @details The imsize of the `value` job is not imported in the containing
#' job.
#'
#' Later on, should check that none of the added nodes have names
#' already present in the job.
#'
#' @export
setMethod("addNode<-", signature(job="ufo.job", name="missing", value="ufo.job"),
          function(job, name, value) {
            if ( 0 != length(intersect(names(job), names(value))) ) {
              warning("Duplicate names in the job and the composite task/sub-job being added. We will not perform the merge")
              return(job)
            }
            job@nodes <- c(job@nodes, value@nodes)
            job@edges <- c(job@edges, value@edges)
            return(job)
          })


#' A function to merge two jobs, or to include a composite task into
#' a larger job.
#'
#' @param job1 One of the two `ufo.job` to be merged
#' @param job2 The other one of the ùfo.job` (to be merged)
#' @param edges A list of the edge that will link `job1` and
#'              `job2` to make it a larger `ufo.job`
#' @return An `ufo.job` containing the union of the nodes of both
#'         `job1` and `job2` with the respective edges as well as
#'         a set of extra edges.
#'
#' @details This function can be used together with function that
#'   are generating composite tasks.
#'
#' Note that both `job1` and `job2` have an attached image size.
#' These will be supposed to be either the same or at least one of
#' them should be empty. Otherwise the `ims.override` is a mandatory
#' argument.
#'
#' Also this function should check that the merger does not produce
#' homonyms between nodes
#'
#' @export
mergeJobs <- function(job1, job2, edges, ims.override) {
    if ( missing(ims.override) ) {
        ims1 <- job1@imsize
        ims2 <- job2@imsize
        if ( 0 == length(ims1@w) ) {
            ims.override <- ims2
        } else {
            if ( ( 0 != length(ims2@w) && any((ims1@w != ims2@w), (ims1@h != ims2@h), (ims1@bd != ims2@bd)) )) {
                stop("Both job1 and job2 have image size, and they differ. This is not handled unless you specify an ims.override.")
            }
            ims.override <- ims1
        }
    }
    uj <- ufo.job(nodes=c(job1@nodes, job2@nodes), edges=c(job1@edges, job2@edges, edges), imsize=ims.override)
    return(uj)
}

#############################################################
## Follow up some functions/methods for generating various ##
## representation of the job                               ##
#############################################################

#' Getting the representation of an ufo job as a list
#'
#' @param x The \code{ufo.job} to be represented as a list
#' @param ... Any ohter parameter required for the list generation
#' @return A list, ready for export as json to use with \code{ufo-runjson}.
#'
#' @export
setMethod("list.representation",
  signature(x="ufo.job"),
  function(x, ...) {
      nodes <- list()
      for (n in x@nodes ) {
          nal <- list.representation(n, job=x)
          nodes[[length(nodes)+1]] <- nal
      }
      edges <- list()
      for (e in x@edges ) {
          eal <- list(from=list(name=e@from), to=list(name=e@to, input=e@index))
          edges[[length(edges)+1]] <- eal
      }
      return(list(nodes=nodes, edges=edges))
  })

#' A function to turn a job description into a json description for ufo
#'
#' @param job Teh job description as a \code{ufo.job} object
#' @return A character string containing the json description of the job,
#'         ready to be used with \code{ufo-runjson}
#'
#' @export
ufo.json.representation <- function(job) {
    return(rjson::toJSON(list.representation(job), indent=2))
}

#' Writing the json file corresponding to a `ufo.job`
#'
#' @param job The `ufo.job` to be exported as a json file
#' @param filename The filename of the generated json representation of the job
#' @return The textual json representation of the `fo.job` as it has been
#'         written in the file
#'
#' @export
ujson.repr <- function(job, filename) {
    tt <- ufo.json.representation(job)
    cat(tt, file=filename)
    return(invisible(tt))
}

#' Running the job, in a subprocess
#'
#' @param job The `ufo.job` to be executed
#' @param scheduler Should `ufo-runjson` use the fixed (default) or
#'        dynamic task scheduler
#' @param trace Should the job enable tracing. Beware the trace file is
#'        generated in the CWD of the R session
#' @param wait A logical (not NA) indicating whether the R interpreter
#'        should wait for the `ufo-runjson` command to finish, or run it
#'        asynchronously.
#' @details This is merely a wrapper that writes the `ufo.job` json representation
#' to a temporary file and then executes `ufo-runjson` with this file as input
#'
#' @export
ufo.run.job <- function(job, scheduler=c("fixed", "dynamic"), trace=FALSE, wait=TRUE, ufo.device=1) {
  scheduler <- match.arg(scheduler)
  jfn <- paste(tempfile(), "json", sep=".")
  ujson.repr(job, jfn)
  uargs <- c("-s ", scheduler, jfn)
  uenv <- sprintf("UFO_DEVICES=%d", ufo.device-1)
  return(system2(command="ufo-runjson", args=uargs, env=uenv, wait=wait, timeout=0))
}


#' Getting the representation of an ufo job as a dot file
#'
#' @param x The \code{ufo.job} to be represented as a dot input string
#' @param ... Any ohter parameter required for the representation generation
#' @return A string, ready for export as json to use with \code{ufo-runjson}.
#'
#' @export
setMethod("dot.representation",
  signature(x="ufo.job"),
  function(x, ...) {
      ndr <- c()
      for (n in x@nodes ) {
          nadr <- dot.representation(n, job=x)
          ndr[length(ndr)+1] <- nadr
      }
      edr <- c()
      for (e in x@edges ) {
          eadr <- sprintf("  \"%s\" -> \"%s\" [penwidth=5 fontsize=28 fontcolor=\"black\" headlabel=\"%d\" ];", e@from, e@to, e@index)
          edr[length(edr)+1] <- eadr
      }
      return(paste("digraph g {",
                    "  graph [fontsize=30 labelloc=\"t\" label=\"\" splines=true overlap=false rankdir = \"LR\"];",
                    "  ratio = auto;",
                    paste(ndr, collapse=""),
                    paste(edr, collapse=""),
                    "}\n",
                    sep="\n"
                    ))
  })

#' A function to write job representation in a dot file and process it to get an image/pdf
#'
#' @param job The ujo.job to be represented
#' @param filename The filename to be used for the `*.dot` file, and the basename for the
#'        generated image file
#' @param type The type of image file to be generated
#' @return The filename of the generated image file
#'
#' @export
dot.repr <- function(job, filename, type=c("pdf", "svg", "png", "ps")) {
    type <- match.arg(type)
    ## filename.dot <- sprintf("%s.%s", filename, "dot")
    filename.dot <- filename
    cat(dot.representation(job), file=filename.dot)
    system(command=sprintf("dot -O -T%s %s", type, filename.dot))
    return(invisible(sprintf("%s.%s", filename, type)))
}
