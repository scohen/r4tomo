/**************************************************************
 c(1)
 * The second line is passed to str2lang to provide the list
 * of arguments of the kernel to fill in which.out of the
 * oclKernel
 **************************************************************/

constant sampler_t nnSampler = CLK_NORMALIZED_COORDS_FALSE |
  CLK_ADDRESS_CLAMP_TO_EDGE |
  CLK_FILTER_NEAREST;

kernel void
norm_Ln_sub ( global float* output,
	      read_only image2d_t i_im1,
	      const int i_n,
	      const int i_x_offset,
	      const int i_y_start,
	      const int i_y_end
	      )
{
  float   acc = 0.0f;
  int     x = get_global_id(0) + i_x_offset;

  for (int y=i_y_start; i_y_end != y; ++y) {
    acc += pown(read_imagef(i_im1, nnSampler, (int2)(x, y)).x
		, i_n);
  }
  output[get_global_id(0)] = acc;
}
