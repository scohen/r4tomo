/**************************************************************
 c(1)
 * The second line is passed to str2lang to provide the list
 * of arguments of the kernel to fill in which.out of the
 * oclKernel
 **************************************************************/

/**************************************************************
 * This file is not present in the package itself, but it
 * is the source to produce the sysdata.rda content of
 * the package
 * (as documented at https://r-pkgs.org/data.html#data-sysdata)
 */

constant sampler_t nnSampler = CLK_NORMALIZED_COORDS_FALSE |
  CLK_ADDRESS_CLAMP_TO_EDGE |
  CLK_FILTER_NEAREST;

/* A kernel computing the Ln energy of an image2d_t
 *
 * In this version, each kernel is computing the sum over the height.
 * In other words, the output should hold enought space to store one
 * numeric value per column (y value) of the image.
 */
kernel void
norm_Ln_full (global float* output,    // a vector of values, one per line
	      read_only image2d_t i_im1, // the image to process
	      const int i_n              // the power used in the norm
	      )
{
  float   acc = 0.0f;
  int     height = get_image_height(i_im1);
  int     x = get_global_id(0);

  for (int y=0; height != y; ++y) {
    acc += pown(read_imagef(i_im1, nnSampler, (int2)(x, y)).x
		, i_n);
  }
  output[x] = acc;
}
