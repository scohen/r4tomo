/**************************************************************
 c(1)
 * The second line is passed to str2lang to provide the list
 * of arguments of the kernel to fill in which.out of the
 * oclKernel
 **************************************************************/


constant sampler_t blSampler = CLK_NORMALIZED_COORDS_FALSE |
  CLK_ADDRESS_CLAMP_TO_EDGE |
  CLK_FILTER_LINEAR;

constant sampler_t nnSampler = CLK_NORMALIZED_COORDS_FALSE |
  CLK_ADDRESS_CLAMP_TO_EDGE |
  CLK_FILTER_NEAREST;

kernel void
dist_Ln_sub ( global float* output,
	      read_only image2d_t i_im1,
	      read_only image2d_t i_im2, // the second image
	      const numeric i_x_flip,    // the x coordinate to the flipping (if <0, no flip applied)
	      const int i_n,
	      const int i_x_offset,
	      const int i_y_start,
	      const int i_y_end
	      )
{
  float   acc = 0.0f;
  float   x = (float)(get_global_id(0) + i_x_offset);
  float   x_2 = (i_x_flip < 0) ? x : 2*i_x_flip - x;
  float   loc_diff;

  float   f_y_start, f_y_end;

  f_y_start = (float)i_y_start;
  f_y_end   = (float)i_y_end;

  for (float y=f_y_start; f_y_end != y; ++y) {
    float2  coords_1 = (float2)(x, y) + .5f;
    float2  coords_2 = (float2)(x_2, y) + .5f;
    loc_diff = fabs(read_imagef(i_im1, nnSampler, coords_1).x - read_imagef(i_im2, blSampler, coords_2).x);
    acc += pown(loc_diff, i_n);
  }
  output[get_global_id(0)] = acc;
}
