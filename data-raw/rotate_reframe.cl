/**************************************************************
 c(1)
 * The second line is passed to str2lang to provide the list
 * of arguments of the kernel to fill in which.out of the
 * oclKernel
 **************************************************************/

/*
 * The rotation is performed using bilinear interpolation
 * clampling to edgewhen outside of input image
 */
constant sampler_t blSampler = CLK_NORMALIZED_COORDS_FALSE |
  CLK_ADDRESS_CLAMP_TO_EDGE |
  CLK_FILTER_LINEAR;


/* o_im The output image
 * i_im The input image
 * i_sin The sinus of the rotation angle
 * i_cos The cosinus of the rotation angle
 * i_center_x The x coordinate of the rotation center in the input image
 * i_center_y The y coordinate of the rotation center in the input image
 * i_padded_center_x The x coordinate of the rotation center in the output image
 * i_padded_center_y The y coordinate of the rotation center in the output image
 */

kernel void
rotate_reframe ( write_only image2d_t o_im,
		 read_only image2d_t i_im,
		 const numeric i_sin,
		 const numeric i_cos,
		 const numeric i_center_x,
		 const numeric i_center_y,
		 const numeric i_padded_center_x,
		 const numeric i_padded_center_y)
{
  int idx = get_global_id (0);
  int idy = get_global_id (1);
  numeric x = (numeric)(idx) - i_padded_center_x + (numeric)(0.5f);
  numeric y = (numeric)(idy) - i_padded_center_y + (numeric)(0.5f);
  float2  coords;
  numeric val;

  coords = (float2) (i_cos * x + i_sin * y + i_center_x,
		     - i_sin * x + i_cos * y + i_center_y );
  val = read_imagef(i_im, blSampler, coords).x;
  write_imagef(o_im, (int2)(idx, idy), (float4)(val, 0.0f, 0.0f, 1.0f));
}
