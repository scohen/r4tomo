/**************************************************************
 c(1)
 * The second line is passed to str2lang to provide the list
 * of arguments of the kernel to fill in which.out of the
 * oclKernel
 **************************************************************/

/**************************************************************
 * OpenCL kernel to compute Ln between two images.
 *
 * This file is not present in the package itself, but it
 * is the source to produce the sysdata.rda content of
 * the package
 * (as documented at https://r-pkgs.org/data.html#data-sysdata)
 */

constant sampler_t blSampler = CLK_NORMALIZED_COORDS_FALSE |
  CLK_ADDRESS_CLAMP_TO_EDGE |
  CLK_FILTER_LINEAR;

constant sampler_t nnSampler = CLK_NORMALIZED_COORDS_FALSE |
  CLK_ADDRESS_CLAMP_TO_EDGE |
  CLK_FILTER_NEAREST;

/* A kernel computing the distance between two image2d_t
 */
kernel void
dist_Ln_full (global numeric* output,    // a vector of values, one per line
	      read_only image2d_t i_im1, // the first input image
	      read_only image2d_t i_im2, // the second image
	      const numeric i_x_flip,    // the x coordinate to the flipping (if <0, no flip applied)
	      const int i_n              // the power used for the norm computation
	      )
{
  float   acc = 0.0f;
  int     height = get_image_height(i_im1);
  float   x = (float)get_global_id(0);
  float   x_2 = (i_x_flip < 0) ? x : 2*i_x_flip - x;
  float   loc_diff;

  for (float y=0.0f; height != y; ++y) {
    float2  coords_1 = (float2)(x, y) + .5f;
    float2  coords_2 = (float2)(x_2, y) + .5f;
    loc_diff = fabs(read_imagef(i_im1, nnSampler, coords_1).x - read_imagef(i_im2, blSampler, coords_2).x);
    acc += pown(loc_diff, i_n);
  }
  output[get_global_id(0)] = acc;
}
