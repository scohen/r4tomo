/**************************************************************
 c(1)
 * The second line is passed to str2lang to provide the list
 * of arguments of the kernel to fill in which.out of the
 * oclKernel
 **************************************************************/

/*
 * A kernel to transform a transmittance image to an abosrbance image
 */

kernel void
trans_to_abs ( write_only image2d_t o_abs_im,
	       read_only image2d_t i_trans_im
	      )
{
  int     idx = get_global_id(0);
  int     idy = get_global_id(1);
  int2    coords = (int2) (idx, idy);
  float4  val = read_imagef(i_trans_im, coords);
  val.x = (val.x > 0.0f) ? -log10(val.x) : 3; // later on, replace by minimal non-negative value in neighbourhood

  write_imagef(o_abs_im, coords, val);
}
