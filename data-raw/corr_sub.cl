/**************************************************************
 c(1, 2, 3)
 * The second line is passed to str2lang to provide the list
 * of arguments of the kernel to fill in which.out of the
 * oclKernel
 **************************************************************/

/*
 * Compute the sigma and covariance of two images
 * The second image might be horizontally flipped.
 *
 * This kernel is used as the final step to compute the
 * cross-correlation of two images (potentially within a ROI)
 */

/*
 * The sampler is used when the second image is flipped, since this
 * can lead to non integer pixel index in the correlation computation
 */

constant sampler_t nnSampler = CLK_NORMALIZED_COORDS_FALSE |
  CLK_ADDRESS_CLAMP_TO_EDGE |
  CLK_FILTER_NEAREST;

constant sampler_t blSampler = CLK_NORMALIZED_COORDS_FALSE |
  CLK_ADDRESS_CLAMP_TO_EDGE |
  CLK_FILTER_LINEAR;

/*
 * o_sig1 the sum over pixels of i_im1 of (x - <x>)^2
 * o_sig2 the sum over pixels of i_im2 of (y - <y>)^2
 * o_cross the sum over pixels of (x - <x>) * (y - <y>)
 * i_im1 the first image
 * i_im2 the second image
 * i_x_flip the x coordinate to the flipping (if <0, no flip applied)
 * i_x_offset the x offset (to compute x index from global_id(0))
 * i_y_start the fist y value within the ROI
 * i_y_end the first y value after the ROI
 */

kernel void
corr_sub ( global float* o_sig1,
	   global float* o_sig2,
	   global float* o_cross,
	   read_only image2d_t i_im1,
	   read_only image2d_t i_im2,
	   const numeric i_mean1,
	   const numeric i_mean2,
	   const numeric i_x_flip,
	   const int i_x_offset,
	   const int i_y_start,
	   const int i_y_end
	   )
{
  const size_t ind = get_global_id(0);

  float acc_1 = 0.0f;
  float acc_2 = 0.0f;
  float acc_cross = 0.0f;

  float x_1 = (float)(ind + i_x_offset);
  float x_2 = (i_x_flip < 0) ? x_1 : 2*i_x_flip - x_1;

  float f_y_start = (float)i_y_start;
  float f_y_end = (float)i_y_end;

  for (float y=f_y_start; f_y_end != y; ++y) {
    float2  coords_1 = (float2)(x_1, y) + .5f;
    float2  coords_2 = (float2)(x_2, y) + .5f;
    float   i1_c = (read_imagef(i_im1, nnSampler, coords_1).x - i_mean1);
    float   i2_c = (read_imagef(i_im2, blSampler, coords_2).x - i_mean2);

    acc_1 += i1_c * i1_c;
    acc_2 += i2_c * i2_c;
    acc_cross += i1_c * i2_c;
  }
  o_sig1[ind] = acc_1;
  o_sig2[ind] = acc_2;
  o_cross[ind] = acc_cross;
}
