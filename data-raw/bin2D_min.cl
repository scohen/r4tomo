/**************************************************************
 c(1)
 * The second line is passed to str2lang to provide the list
 * of arguments of the kernel to fill in which.out of the
 * oclKernel
 **************************************************************/

// Defining some constant to (as options to the compilation) :
// EDGE : the size of the box edge size, in pixel/voxel count

/**************************************************************
 * Performing binning by computing the mean value of the pixels
 * in a box of edge size EDGE (a macro that has to be defined
 * in the code before compilation)
 *
 * This kernel is run using one WorkItem per pixel in the
 * output image (the o_bin_im 2d image). For each of those
 * pixels/work item the full binned boxed should be present in
 * the input image, otherwise the values are produced by the
 * sampler used to get values from input image.
 **************************************************************/


kernel void
bin2D_min ( write_only image2d_t o_bin_im
	    , read_only image2d_t i_full_im
	    , const float one_on_n
	    )
{
  // Sampler used for image access :
  const sampler_t samp=CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

  int        o_idx = get_global_id(0);
  int        o_idy = get_global_id(1);
  int2       o_coords = (int2) (o_idx, o_idy);

  const int  i_xstart = EDGE * o_idx;
  const int  i_xend = i_xstart + EDGE;
  const int  i_ystart = EDGE * o_idy;
  const int  i_yend = i_ystart + EDGE;
  int2       i_coords = (int2) (i_xstart, i_ystart);

  float      min_val;
  float4     val;

  val = read_imagef(i_full_im, samp, i_coords);
  min_val = val.x;

  for (int iiX=i_xstart; i_xend != iiX; ++iiX) {
    for (int iiY=i_ystart; i_yend != iiY; ++iiY) {
      i_coords = (int2) (iiX, iiY);

      val = read_imagef(i_full_im, samp, i_coords);
      min_val = fmin(min_val, val.x);
    }
  }
  val.x = min_val;
  write_imagef(o_bin_im, o_coords, val);
}
