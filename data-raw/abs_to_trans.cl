/**************************************************************
 c(1)
 * The second line is passed to str2lang to provide the list
 * of arguments of the kernel to fill in which.out of the
 * oclKernel
 **************************************************************/

/*
 * A kernel to transform an absorbance image to a transmittance image
 */

kernel void
abs_to_trans ( write_only image2d_t o_trans_im,
	       read_only image2d_t i_abs_im
	      )
{
  int     idx = get_global_id(0);
  int     idy = get_global_id(1);
  int2    coords = (int2) (idx, idy);
  float4  val = read_imagef(i_abs_im, coords);
  val.x = exp10(-val.x);

  write_imagef(o_trans_im, coords, val);
}
