/**************************************************************
 c(1)
 * The second line is passed to str2lang to provide the list
 * of arguments of the kernel to fill in which.out of the
 * oclKernel
 **************************************************************/

/*
  Compute the flat field corrected (also known as non-uniformity corrected)
  image of a radiograph, potentially expressed as an absorbance image.

  This is related to the ffc.cl kernel of UFO, trying to get results as
  close as possible to the one computed by UFO.
*/

kernel void
flat_field_correct ( write_only image2d_t o_corrected,
		     read_only image2d_t i_radio,
		     read_only image2d_t i_dark,
		     read_only image2d_t i_bright,
		     const int absorbance,
		     const int fix_abnormal,
		     const float dark_scale,
		     const float bright_scale
		     )
{
  const int     idx = get_global_id(0);
  const int     idy = get_global_id(1);
  int2          coords = (int2) (idx, idy);
  // Notice that image needs float4 for both read and write :
  // hence keep the same one from the radio readin to the corrected image writeout.
  float4        val_rr = read_imagef(i_radio, coords);
  const float   cflat = read_imagef(i_bright, coords).x * bright_scale;
  const float   cdark = read_imagef(i_dark, coords).x * dark_scale;
  float         result;

  if (absorbance) {
    result = log((cflat - cdark) / (val_rr.x - cdark));
  }
  else {
    result = (val_rr.x - cdark) / (cflat - cdark);
  }

  if (fix_abnormal && (isnan (result) || isinf (result))) {
    result = 0.0f;
  }
  val_rr.x = result;
  write_imagef(o_corrected, coords, val_rr);
}
